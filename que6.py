import itertools
import sys
def func(nums):
    summ = sum(nums)
    if(summ % 2520 == 0):
        return True
    return False


N = int(input())
if (N>=1) and (N<=1000):
    numbers = [int(x) for x in input().split() if (int(x)>=1) and (int(x)<=10**9)]
    if len(numbers)==N:
        times = 0
        nums = []
        for num in set(numbers):
            if num % 2520 == 0:
                times = times +1
            else:
                nums.append(num)
        del numbers[:]
        count=0
        checklist=set([])
        for i in range(0, N):
            subset = [num for num in set(itertools.combinations(nums,i)) if any(set(s).issubset(num) for s in checklist )==False]
            for j in range(0,len(subset)):
                if func(subset[j]):
                    count = count + 1
                    checklist.add(subset[j])
                    count=(2**count)-1
        times=(2**times)-1
        print(((times*count)+times+count)%(10**9))

