import itertools

n, a, b, c = input().split()
n, a, b, c = int(n), int(a), int(b), int(c)
i = 0
boxes = []
roomVol = a * b * c
while i < n :
    x, y, z = input().split()
    x, y, z = int(x), int(y), int(z)
    if (x >= 1 and x <= 100) and (y >= 1 and y <= 100) and (z >= 1 and z <= 100):
        if (x*y*z)<roomVol:
            boxes.append([x, y, z])
    i = i + 1

def isFitting(b1, b2):
    for abc in set(itertools.permutations((a,b,c), 3)):
        for s1 in set(itertools.permutations(b1, 3)):
            for s2 in set(itertools.permutations(b2,3)):
                if ((s1[0] + s2[0]) <= abc[0]) and ((s1[1]<=abc[1]) and (s2[1]<=abc[1])) and ((s1[2]<=abc[2]) and (s2[2]<=abc[2])):
                    return True
    return False

maxVol = 0
for i in range(0, len(boxes)):
    for j in range(i+1, len(boxes)):
        if isFitting(boxes[i],boxes[j]):
            v1 = boxes[i][0] *  boxes[i][1] * boxes[i][2]
            v2 = boxes[j][0] *  boxes[j][1] * boxes[j][2]
            if (v1 + v2) > maxVol and  (v1 + v2) <= roomVol:
                maxVol = v1 + v2

print(maxVol)
