X, N, K = [int(x) for x in input().split()]
if (X >= 1 and X <= 9) and N >= 1 and K >=1  and ((N+K) <= 10**6):
    X=int(str(X)*N)
    A=(X*10**(K)-X)/9
    print(int(A))
else:
    print(0)
