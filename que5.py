import itertools

def isSame(possible,mycube):
    cube1=(mycube[4],mycube[3],mycube[5],mycube[1],mycube[0],mycube[2])
    if cube1 in possible and not cube1==mycube:
        possible.remove(cube1)
    cube1=(mycube[0],mycube[3],mycube[2],mycube[1],mycube[5],mycube[4])
    if cube1 in possible and not cube1==mycube:
        possible.remove(cube1)
    cube1=(mycube[3],mycube[5],mycube[1],mycube[4],mycube[0],mycube[2])
    if cube1 in possible and not cube1==mycube:
        possible.remove(cube1)
    cube1=(mycube[1],mycube[4],mycube[3],mycube[5],mycube[0],mycube[2])
    if cube1 in possible and not cube1==mycube:
        possible.remove(cube1)
    cube1=(mycube[1],mycube[0],mycube[3],mycube[2],mycube[5],mycube[4])
    if cube1 in possible and not cube1==mycube:
        possible.remove(cube1)
    cube1=(mycube[2],mycube[4],mycube[0],mycube[5],mycube[1],mycube[3])
    if cube1 in possible and not cube1==mycube:
        possible.remove(cube1)
    cube1=(mycube[1],mycube[5],mycube[3],mycube[4],mycube[2],mycube[0])
    if cube1 in possible and not cube1==mycube:
        possible.remove(cube1)
    cube1=(mycube[3],mycube[2],mycube[1],mycube[0],mycube[5],mycube[4])
    if cube1 in possible and not cube1==mycube:
        possible.remove(cube1)
    cube1=(mycube[5],mycube[2],mycube[4],mycube[0],mycube[1],mycube[3])
    if cube1 in possible and not cube1==mycube:
        possible.remove(cube1)
    cube1=(mycube[1],mycube[2],mycube[3],mycube[0],mycube[4],mycube[5])
    if cube1 in possible and not cube1==mycube:
        possible.remove(cube1)
    cube1=(mycube[4],mycube[2],mycube[5],mycube[0],mycube[3],mycube[1])
    if cube1 in possible and not cube1==mycube:
        possible.remove(cube1)
    cube1=(mycube[5],mycube[3],mycube[4],mycube[1],mycube[2],mycube[0])
    if cube1 in possible and not cube1==mycube:
        possible.remove(cube1)
    cube1=(mycube[5],mycube[0],mycube[4],mycube[2],mycube[3],mycube[1])
    if cube1 in possible and not cube1==mycube:
        possible.remove(cube1)
    cube1=(mycube[0],mycube[5],mycube[2],mycube[4],mycube[1],mycube[3])
    if cube1 in possible and not cube1==mycube:
        possible.remove(cube1)
    cube1=(mycube[0],mycube[4],mycube[2],mycube[5],mycube[3],mycube[1])
    if cube1 in possible and not cube1==mycube:
        possible.remove(cube1)
    cube1=(mycube[4],mycube[1],mycube[5],mycube[3],mycube[2],mycube[0])
    if cube1 in possible and not cube1==mycube:
        possible.remove(cube1)
    cube1=(mycube[0],mycube[1],mycube[2],mycube[3],mycube[4],mycube[5])
    if cube1 in possible and not cube1==mycube:
        possible.remove(cube1)
    cube1=(mycube[2],mycube[5],mycube[0],mycube[4],mycube[3],mycube[1])
    if cube1 in possible and not cube1==mycube:
        possible.remove(cube1)
    cube1=(mycube[2],mycube[1],mycube[0],mycube[3],mycube[5],mycube[4])
    if cube1 in possible and not cube1==mycube:
        possible.remove(cube1)
    cube1=(mycube[2],mycube[3],mycube[0],mycube[1],mycube[4],mycube[5])
    if cube1 in possible and not cube1==mycube:
        possible.remove(cube1)
    cube1=(mycube[4],mycube[0],mycube[5],mycube[2],mycube[1],mycube[3])
    if cube1 in possible and not cube1==mycube:
        possible.remove(cube1)
    cube1=(mycube[3],mycube[0],mycube[1],mycube[2],mycube[4],mycube[5])
    if cube1 in possible and not cube1==mycube:
        possible.remove(cube1)
    cube1=(mycube[5],mycube[1],mycube[4],mycube[3],mycube[0],mycube[2])
    if cube1 in possible and not cube1==mycube:
        possible.remove(cube1)
    cube1=(mycube[3],mycube[4],mycube[1],mycube[5],mycube[2],mycube[0])
    if cube1 in possible and not cube1==mycube:
        possible.remove(cube1)


N = int(input())
if N>=1 and N<=25:
    colors=[int(x) for x in input().split()]
    if len(colors)==N:
        if len(set(colors))==len(colors):
            print((len(list(itertools.combinations(range(N),6)))*6*5))
        else:
            colors=list(set(itertools.permutations(colors,6)))
            for elem in colors:
                isSame(colors,elem)
            print(len(colors))
